//
//  main.m
//  ObjectiveC
//
//  Created by Minbok.Choi on 2017. 7. 3..
//  Copyright © 2017년 KDS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
